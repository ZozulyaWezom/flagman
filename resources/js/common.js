import 'jquery-inview';
import 'jquery';
import 'bootstrap';

class Slider {
    constructor ($targerElem, options) {
        this.defaultOptions = {
            infinite: true,
            autoplay: true,
        };
        this.options = Object.assign({}, this.defaultOptions, options);
        $targerElem.slick(this.options);
    }
}

class Counter {
    constructor($elem, duration) {
        this.start = false;
        this.$elem = $elem;
        this.duration = duration;
        this.$elem.on('inview', (e, isInView) => this.go(isInView));
    }
    go(isInView) {
        if (isInView) {
            this._counterStart();
        }
    }
    _counterStart() {
        if (!this.start) {
            this.$elem.prop('Counter', 0).animate({
                Counter: this.$elem.text()
            }, {
                duration: this.duration,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        }
        this.start = true;
    }
}

$(document).ready(function () {

    new Slider ( $('.js-header-slider'), {
        dots: true,
        arrows: false,
        draggable: false,
        customPaging : function(slider, i) {
            return '<div href="#" class="header-slider__dot"></div>';
        }
    });

    new Slider ( $('.js-news-slider'), {
        arrows: true,
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true,
        prevArrow: $('.news-arrows__left'),
        nextArrow: $('.news-arrows__right'),
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    arrows: false,
                    centerMode: false,
                    variableWidth: false,
                }
            }
        ]
    });

    // counters
    const $counter = $('.js-count');
    $counter.each( (_, elem) => {
        new Counter($(elem), 2000);
    });

    // scroll to top
    $('.js-footer__arrow').on('click', () => {
        $("html, body").animate({ scrollTop: 0 }, "slow");
    });

});
