<div class="header-navbar">
    <div class="container-block">
        <nav class="navbar navbar-expand-lg ">
            <a class="logo logo-header" href="/">
                <img src="{{ asset('/img/logo.png') }}" alt="logo">
                <div class="logo-header__descr">repair of ships</div>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header-navbar">
                <div class="navbar-toggler-icon">
                    <div class="header-navbar__line"></div>
                    <div class="header-navbar__line"></div>
                    <div class="header-navbar__line"></div>
                </div>
            </button>
            <div class="collapse navbar-collapse" id="header-navbar">
                <ul class="navbar-nav navbar-nav-header">
                    <li class="nav-item">
                        <a class="nav-link header-navbar__link" href="#">Home </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link header-navbar__link" href="#">About us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link header-navbar__link" href="#">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link header-navbar__link" href="#">News</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link header-navbar__link" href="#">Contact</a>
                    </li>
                </ul>
                <div class="phone">
                    <div>
                        <img src="{{ asset('/img/phone_icon.png') }}" alt="">
                    </div>
                    <div class="dropdown">
                        <div class="phone" data-toggle="dropdown">
                            +38 (048) 729 61 61
                        </div>
                        <div class="dropdown-menu phone__numbers">
                            <a href="tel:+38(048)7296161" class="dropdown-item phone__link">+38 (048) 729 61 61</a>
                            <a href="tel:+38(048)7296161" class="dropdown-item phone__link">+38 (048) 729 61 61</a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>
