<div class="footer">
    <div class="footer__line">
        <div class="container-block">
            <nav class="navbar footer-navbar navbar-expand-lg">
                <a class="logo logo-footer" href="#">
                    <img src="{{ asset('/img/logo.png') }}" alt="logo">
                </a>
                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav navbar-nav-header">
                        <li class="nav-item active">
                            <a class="nav-link footer__nav-link" href="#">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link footer__nav-link" href="#">About us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link footer__nav-link" href="#">Services</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link footer__nav-link" href="#">News</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link footer__nav-link" href="#">Contact</a>
                        </li>
                    </ul>
                </div>
                <div>
                    <div class="footer-arrow-cont">
                        <svg class="footer__arrow js-footer__arrow" transform="rotate(90)" version="1.1"
                             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                             viewBox="0 0 31.494 31.494" style="enable-background:new 0 0 31.494 31.494;"
                             xml:space="preserve">
                            <path d="M10.273,5.009c0.444-0.444,1.143-0.444,1.587,0c0.429,0.429,0.429,1.143,0,1.571l-8.047,8.047h26.554
                            c0.619,0,1.127,0.492,1.127,1.111c0,0.619-0.508,1.127-1.127,1.127H3.813l8.047,8.032c0.429,0.444,0.429,1.159,0,1.587
                            c-0.444,0.444-1.143,0.444-1.587,0l-9.952-9.952c-0.429-0.429-0.429-1.143,0-1.571L10.273,5.009z"/>
                        </svg>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <div class="container-block">
        <div class="footer-footer">
            <div class="footer-footer__cop">
                © 2007-2018 Flagman Ukraine
            </div>
            <div>
                <a href="#" class="footer-footer__w">
                    <img src="{{ asset('/img/w.png') }}" alt="w">
                    <div class="footer-footer__w-pad">Wezom</div>
                </a>
            </div>
        </div>
    </div>
</div>
