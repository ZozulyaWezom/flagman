@extends('layouts.default')
@section('content')
    <div class="header-slider js-header-slider">
        @foreach( config('dataTest') -> headerSlider as $slide)
            <div class="header-slider-slide blue-grad"  style="background-image: url('{{ asset($slide -> imageURL) }}')">
                    <div class="header-slider-slide__descr">
                        <div class="header-slider-slide__text">{{ $slide -> descr }}</div>
                        <a class="header-slider-slide__link link-btn">DISCOVER</a>
                    </div>
            </div>
        @endforeach
    </div>
    <div class="services">
        <div class="container container-block">
            <div class="title title-center">
                <div class="title-underline">Our services</div>
            </div>
            <div class="services-items container container-block">
                <div class="row">
                    @foreach( config('dataTest') -> services as $item )
                        <div class="services-item col-12 col-md-6 col-lg-4">
                            <div class="services-item__img blue-grad"
                                 style="background-image: url({{$item->imageURL}})">
                                <div class="services-item__description">
                                    <div class="services-item__title">{{ $item->name }}</div>
                                    <a href="{{ $item->link }}" class="services-item__link">DISCOVER</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="container container-block">
        <div class="counters row">
            @foreach( config('dataTest') -> counters as $counts)
                <div class="counters-item col-12 col-md-4">
                    <div class="counters__count js-count">{{ $counts -> count }}</div>
                    <div class="counters__descr">{{ $counts -> descr }}</div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="news">
        <div class="news-title-block container-block">
            <div class="news-title-block__title title title-white title-center">News</div>
            <a class="news-title-block__all" href="#">All news</a>
        </div>
        <div class="news-slider js-news-slider">
            @foreach(config('dataTest') -> newsSlider as $slide)
                <div class="news-slider-slide blue-grad">
                    <img class="img-fluid" src="{{ asset($slide -> imageURL) }}" alt="alt">
                    <div class="news-slider-slide__descr">
                        <div class="news-slider-slide__text">{{$slide -> descr}}</div>
                        <a class="news-slider-slide__link" href="#">Read more</a>
                    </div>
                    <div class="news-slider-slide__date">{{$slide -> date}}</div>
                </div>
            @endforeach
        </div>
        <div class="news-arrows paginator-center">
            <svg class="news-arrow news-arrows__left" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                 viewBox="0 0 31.494 31.494" style="enable-background:new 0 0 31.494 31.494;" xml:space="preserve">
            <path d="M10.273,5.009c0.444-0.444,1.143-0.444,1.587,0c0.429,0.429,0.429,1.143,0,1.571l-8.047,8.047h26.554
            c0.619,0,1.127,0.492,1.127,1.111c0,0.619-0.508,1.127-1.127,1.127H3.813l8.047,8.032c0.429,0.444,0.429,1.159,0,1.587
            c-0.444,0.444-1.143,0.444-1.587,0l-9.952-9.952c-0.429-0.429-0.429-1.143,0-1.571L10.273,5.009z"/>
            </svg>
            <svg class="news-arrow news-arrows__right" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                 x="0px" y="0px"
                 viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve">
            <path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111
                C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587
                c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z"/>
            </svg>
        </div>
    </div>

    <div class="company">
        <div class="container container-block">
            <div class="row">
                <div class="col-12 col-lg-7">
                    <div class="company__img-wrapper">
                        <img class="company__img" src="{{ asset('/img/company.jpg') }}" alt="">
                    </div>
                </div>
                <div class="col-12 col-lg-5">
                    <div class="company-descr d-flex flex-column">
                        <div class="title title-center title-padding title-left title-underline title-underline-left">
                            About company Flagman
                        </div>
                        <div class="company-descr-items">
                            <p class="company-descr__item">Thank you for your interest in our company, established and
                                dedicated for providing Ukrainian ship repair squads and competent crews to ship owners,
                                operators and shipyards worldwide.</p>
                            <p class="company-descr__item">Our mission is to provide our customers with high quality,
                                cost effective and personalized services which will support and encourage safe and
                                efficient ship operation.</p>
                            <p class="company-descr__item">We look forward to being of service to you and we are sure we
                                will be able not only to meet your requirements but also to exceed your
                                expectations.</p>
                            <p class="company-descr__item">Quality Commitment of Flagman Ukraine.</p>
                            <p class="company-descr__item">Our Quality Management System has been certified to ISO
                                9001:2000 standard by BVQI.</p>
                            <a href="#" class="link-btn company__link">DISCOVER</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
