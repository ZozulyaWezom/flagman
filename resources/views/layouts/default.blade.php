<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>
<div class="wrapper">
    <header>
        @include('includes.header')
    </header>

    <div class="main" class="row">
        @yield('content')
    </div>

    <footer>
        @include('includes.footer')
    </footer>

</div>
<script src="{{ mix('js/manifest.js') }}"></script>
<script src="{{ mix('js/vendor.js') }}"></script>
<script src="{{ mix('js/app.js') }}"></script>
<script src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="{{ mix('js/common.js') }}"></script>
</body>
</html>
