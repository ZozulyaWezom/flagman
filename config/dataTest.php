<?php

$data = [
    'headerSlider' => [
        [
           'descr' => 'Repair of container trucks',
            'imageURL' => '/img/slide.jpg'
        ],
        [
            'descr' => 'Repair of cruise liners',
            'imageURL' => '/img/slide_2.jpg'
        ]
    ],
    'newsSlider' => [
        [
            'descr' => 'The Transport Committee is ready to
                    compromise on the admission of foreign...',
            'date' => '28.06.2018',
            'imageURL' => '/img/news_slider_1.jpg'
        ],
        [
            'descr' => 'Testing life-saving appliances',
            'date' => '20.06.2018',
            'imageURL' => '/img/news_slider_2.jpg'
        ],
        [
            'descr' => 'International Far East Marine Salon 2018 will be held in Vladivostok',
            'date' => '28.06.2018',
            'imageURL' => '/img/news_slider_3.jpg'
        ]
    ],
    'services' => [
        [
            'name' => 'Steel Repairs',
            'imageURL' => '/img/services_1.jpg',
            'link' => 'someurl'
        ],
        [
            'name' => 'Blasting and Painting',
            'imageURL' => '/img/services_2.jpg',
            'link' => 'someurl'
        ],
        [
            'name' => 'Pipework Repair',
            'imageURL' => '/img/services_3.jpg',
            'link' => 'someurl'
        ],
        [
            'name' => 'Machinery Overhauls',
            'imageURL' => '/img/services_4.jpg',
            'link' => 'someurl'
        ],
        [
            'name' => 'Engine Repairs',
            'imageURL' => '/img/services_5.jpg',
            'link' => 'someurl'
        ],
        [
            'name' => 'Electrical Installations',
            'imageURL' => '/img/services_6.jpg',
            'link' => 'someurl'
        ]
    ],
    'counters' => [
        [
            'count' => '10',
            'descr' => 'Unique services'
        ],
        [
            'count' => '600',
            'descr' => 'Completed licensed activities'
        ],
        [
            'count' => '30',
            'descr' => 'Certified workers'
        ]
    ]
];

return json_decode(json_encode($data), FALSE);
